import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Animal } from '../models/animal.model';
import { AnimalService } from '../providers/animal.service';

@Component({
  selector: 'app-animals',
  templateUrl: './animals.component.html',
  styleUrls: ['./animals.component.css']
})
export class AnimalsComponent implements OnInit {
  animals?: Animal[];
  searchOwner: string = '';
  animal!: Animal;
  id: string = "";
  isEditing: boolean = false;

  constructor(private animalService: AnimalService, private router: Router) { }

  ngOnInit() {
    this.getAnimals();
  }

  getAnimals(): void {
    this.animalService.getAllAnimals()
      .subscribe(animals => this.animals = animals);
  }
  onClick(id: string) {
    console.log("in onclick " + id + "|");
    this.router.navigate(['/details'],
      {
        queryParams: {
          id: id
        }
      });
  }

  addAnimal() {
    console.log("in addAnimal ");
    this.router.navigate(['/add'],
      {
        queryParams: {
        }
      });
  }
  getAnimalsByOwner() {
    if (!this.searchOwner) {
      this.getAnimals();
      return;
    }
    this.animalService.getAnimalsByOwner(this.searchOwner)
      .subscribe(data => {
        this.animals = data;
      });
  }
  
  
}