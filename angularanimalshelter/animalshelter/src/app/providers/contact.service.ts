import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  private apiUrl = 'https://example.com/api/contact';

  constructor(private http: HttpClient) { }

  sendMessage(name: string, email: string, message: string): Observable<any> {
    const formData = new FormData();
    formData.append('name', name);
    formData.append('email', email);
    formData.append('message', message);
    return this.http.post<any>(this.apiUrl, formData);
  }
}
