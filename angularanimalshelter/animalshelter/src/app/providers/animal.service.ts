import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Animal } from '../models/animal.model';

@Injectable({
  providedIn: 'root'
})
export class AnimalService {
  private CoursesEndpoint: string = 'https://localhost:7227/api/animals' ;
  private httpOptions = {
    headers: new HttpHeaders (
      {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin' : "*"
      }
    )
  }
  constructor(private http: HttpClient) { }

  getAllAnimals(): Observable<Animal[]> {
    return this.http.get<Animal[]>(`${this.CoursesEndpoint}`, this.httpOptions)
    .pipe(map((res) => <Animal[]>res));
  }

  getAnimalById(id: number): Observable<Animal> {
    return this.http.get<Animal>(`${this.CoursesEndpoint}/${id}`, this.httpOptions);
  }

  getAnimalsByOwner(owner: string): Observable<Animal[]> {
    return this.http.get<Animal[]>(`${this.CoursesEndpoint}/owner/${owner}`, this.httpOptions);
  }

  addAnimal(animal: Animal): Observable<Animal> {
    return this.http.post<Animal>(`${this.CoursesEndpoint}`, animal, this.httpOptions);
  }

  updateAnimal(animal: Animal): Observable<any> {
    return this.http.put(`${this.CoursesEndpoint}/${animal.id}`, animal, this.httpOptions);
  }

  deleteAnimal(id: number): Observable<any> {
    return this.http.delete(`${this.CoursesEndpoint}/${id}`, this.httpOptions);
  }
}

