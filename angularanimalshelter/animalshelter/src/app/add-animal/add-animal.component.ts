import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

import { Animal } from '../models/animal.model';
import { AnimalService } from '../providers/animal.service';

@Component({
  selector: 'app-add-animal',
  templateUrl: './add-animal.component.html',
  styleUrls: ['./add-animal.component.css']
})
export class AddAnimalComponent implements OnInit {

  animal: Animal = new Animal(0, '', '', '', '','','');
  submitted = false;

  constructor(private animalService: AnimalService) { }

  ngOnInit(): void {
  }

  saveAnimal(): void {
    this.animalService.addAnimal(this.animal)
      .subscribe(
        response => {
          console.log(response);
          this.submitted = true;
        },
        error => {
          console.log(error);
        });
    this.animal = new Animal(0, '', '', '', '','','');
  }

  onSubmit(): void {
    this.saveAnimal();
  }
}
