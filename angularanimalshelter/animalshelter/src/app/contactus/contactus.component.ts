import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ContactService } from '../providers/contact.service';

@Component({
  selector: 'app-contact-us',
  templateUrl: 'contactus.component.html',
  styleUrls: ['contactus.component.css']
})
export class ContactusComponent {
  name: string = '';
  email: string = '';
  message: string = '';

  constructor(private contactService: ContactService) { }

  onSubmit(formData: any) {
    this.contactService.sendMessage(this.name, this.email, this.message).subscribe(response => {
      console.log(response);
      formData.reset();
    });
  }
}
