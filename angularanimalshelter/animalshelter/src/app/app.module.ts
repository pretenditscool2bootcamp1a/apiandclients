import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AnimalsComponent } from './animals/animals.component';
import { AnimalsDetailsComponent } from './animals-details/animals-details.component';
import { AddAnimalComponent } from './add-animal/add-animal.component';
import { ImageSliderComponent } from './image-slider/image-slider.component';
import { LearnmoreComponent } from './learnmore/learnmore.component';
import { ContactusComponent } from './contactus/contactus.component';

const appRoutes: Routes = [
  { path: "", component: HomeComponent },
  { path: "all", component: AnimalsComponent},
  { path: "details", component: AnimalsDetailsComponent },
  { path: "add", component: AddAnimalComponent},
  { path: "learnmore", component: LearnmoreComponent},
  { path: "contactus", component: ContactusComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AnimalsComponent,
    AnimalsDetailsComponent,
    AddAnimalComponent,
    ImageSliderComponent,
    LearnmoreComponent,
    ContactusComponent
  ],
  imports: [
    BrowserModule, 
    RouterModule.forRoot(appRoutes), 
    FormsModule, 
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
