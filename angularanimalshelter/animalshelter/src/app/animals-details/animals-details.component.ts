import { ThisReceiver } from '@angular/compiler';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Animal } from '../models/animal.model';
import { AnimalService } from '../providers/animal.service';

@Component({
  selector: 'app-animals-details',
  templateUrl: './animals-details.component.html',
  styleUrls: ['./animals-details.component.css']
})
export class AnimalsDetailsComponent {
  animal!: Animal;
  id: string = "";
  isEditing: boolean = false;
  checkin: string = '';
  checkout: string = '';
  constructor(private activatedRoute: ActivatedRoute, private animalService: AnimalService, private router: Router) { }

  ngOnInit() {
    this.checkin = '';
    this.checkout = '';
    this.activatedRoute.queryParams.subscribe(params => {
      this.id = params["id"];
      console.log("catch passed param" + this.id);
    });
    this.animalService.getAnimalById(+this.id)
      .subscribe(data => {
        this.animal = data;
        if (this.animal.checkInDate !== null) {
          this.checkin = this.animal.checkInDate.split("T")[0];
        }
        if (this.animal.checkOutDate !== null) {
          this.checkout = this.animal.checkOutDate.split("T")[0];
        }
      })
  }
  onSubmit() {
    console.dir(this.animal);
    this.animal.checkInDate = this.checkin;
    this.animal.checkOutDate = this.checkout;
    this.animalService.updateAnimal(this.animal)
      .subscribe(animal => {
        console.log('animal updated: ', animal);
        this.isEditing = false;
        this.router.navigate(['/all']);
      });

  }
  onDelete() {
    if (confirm('Are you sure you want to delete this course?')) {
      this.animalService.deleteAnimal(+this.id)
        .subscribe(() => {
          console.log('Animal deleted: ', this.animal);
          this.router.navigate(['/all']);
        });
    }
  }
  onEdit() {
    this.isEditing = !this.isEditing;
  }

  onCancel() {
    this.isEditing = false;

  }


}
