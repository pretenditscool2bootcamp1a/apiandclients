import { Component } from '@angular/core';

@Component({
  selector: 'app-image-slider',
  template: `
    <img [src]="currentImage" alt="slideshow" class="img-fluid">
  `,
  styleUrls: ['./image-slider.component.css']
})
export class ImageSliderComponent {
  images: string[] = [
    'https://static.pexels.com/photos/290263/pexels-photo-290263.jpeg',
    'https://upload.wikimedia.org/wikipedia/en/a/a2/Golden_Labrador_Toby_2007.jpg',
    'https://upload.wikimedia.org/wikipedia/commons/f/fd/Red-browed_Amazon_parrot.jpg'
  ];
  currentImage: string = this.images[0];

  ngOnInit() {
    setInterval(() => {
      let currentIndex = this.images.indexOf(this.currentImage);
      if (currentIndex == this.images.length - 1) {
        currentIndex = 0;
      } else {
        currentIndex++;
      }
      this.currentImage = this.images[currentIndex];
    }, 5000);
  }
}
