export class Animal {

   constructor ( public id: number,
    public name: string,
    public species: string,
    public breed: string,
    public owner: string,
    public checkInDate: String,
    public checkOutDate: String){}
  }
  
