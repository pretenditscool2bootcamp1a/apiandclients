﻿INSERT INTO animal (Name, Species, Breed, Owner, CheckInDate, CheckOutDate)
VALUES 
  ('Fluffy', 'Cat', 'Siamese', 'John Doe', '2023-04-05 10:00:00', '2023-04-06 10:00:00'),
  ('Buddy', 'Dog', 'Golden Retriever', 'Jane Smith', '2023-04-05 11:00:00', '2023-04-06 11:00:00'),
  ('Luna', 'Cat', 'Calico', 'Bob Johnson', '2023-04-05 12:00:00', '2023-04-06 12:00:00'),
  ('Max', 'Dog', 'German Shepherd', 'Alice Green', '2023-04-05 13:00:00', '2023-04-06 13:00:00'),
  ('Rocky', 'Hamster', 'Syrian', 'Emily Brown', '2023-04-05 14:00:00', '2023-04-06 14:00:00'),
  ('Snowball', 'Rabbit', 'Dwarf', 'William Lee', '2023-04-05 15:00:00', '2023-04-06 15:00:00'),
  ('Shadow', 'Cat', 'Russian Blue', 'Samantha Jones', '2023-04-05 16:00:00', '2023-04-06 16:00:00'),
  ('Simba', 'Lion', 'African Lion', 'Zoey Taylor', '2023-04-05 17:00:00', '2023-04-06 17:00:00'),
  ('Charlie', 'Horse', 'Arabian', 'Michael Davis', '2023-04-05 18:00:00', '2023-04-06 18:00:00'),
  ('Ginger', 'Guinea Pig', 'American', 'Sophie Wilson', '2023-04-05 19:00:00', '2023-04-06 19:00:00'),
  ('Nessie', 'Loch Ness Monster', 'Unknown', 'Sarah Thompson', '2023-04-05 20:00:00', '2023-04-06 20:00:00'),
  ('Bigfoot', 'Sasquatch', 'Unknown', 'Kevin Chen', '2023-04-05 21:00:00', '2023-04-06 21:00:00'),
  ('Chupacabra', 'Goat Sucker', 'Unknown', 'Ella Martinez', '2023-04-05 22:00:00', '2023-04-06 22:00:00'),
  ('Mothman', 'Mothman', 'Unknown', 'Oliver Garcia', '2023-04-05 23:00:00', '2023-04-06 23:00:00'),
  ('Kraken', 'Giant Squid', 'Unknown', 'Ava Nguyen', '2023-04-06 00:00:00', '2023-04-07 00:00:00');
