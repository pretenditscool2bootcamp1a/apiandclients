﻿CREATE TABLE animal (
  id INT identity PRIMARY KEY,
  Name VARCHAR(255),
  Species VARCHAR(255),
  Breed VARCHAR(255),
  Owner VARCHAR(255),
  CheckInDate DATETIME,
  CheckOutDate DATETIME
);