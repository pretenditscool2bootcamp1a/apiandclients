﻿using AnimalShelter.Models;
using Microsoft.EntityFrameworkCore;

namespace AnimalShelter.Repository
{
    public class AnimalDataLayer
    {
        private readonly AnimalShelterDbContext _context;

        public AnimalDataLayer(AnimalShelterDbContext context)
        {
            _context = context;
        }

        public async Task<List<Animal>> GetAnimals()
        {
            return await _context.Animals.ToListAsync();
        }

        public async Task<Animal> GetAnimal(int id)
        {
            return await _context.Animals.FindAsync(id);
        }

        public async Task<List<Animal>> GetAnimalsByOwner(string owner)
        {
            return await _context.Animals.Where(a => a.Owner.ToLower().Contains(owner.ToLower())).ToListAsync();
        }

        public async Task<int> AddAnimal(Animal animal)
        {
            _context.Animals.Add(animal);
            await _context.SaveChangesAsync();

            return animal.Id;
        }

        public async Task<bool> UpdateAnimal(Animal animal)
        {
            _context.Entry(animal).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AnimalExists(animal.Id))
                {
                    return false;
                }
                else
                {
                    throw;
                }
            }

            return true;
        }

        public async Task<bool> DeleteAnimal(int id)
        {
            var animal = await _context.Animals.FindAsync(id);
            if (animal == null)
            {
                return false;
            }

            _context.Animals.Remove(animal);
            await _context.SaveChangesAsync();

            return true;
        }

        private bool AnimalExists(int id)
        {
            return _context.Animals.Any(e => e.Id == id);
        }
    }

}
