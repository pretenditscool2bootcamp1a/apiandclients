﻿using AnimalShelter.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace AnimalShelter.Repository
{
    public class AnimalDbMethods
    {
        private readonly AnimalShelterDbContext _context;

        public AnimalDbMethods(AnimalShelterDbContext context)
        {
            _context = context;
        }

        public async Task<ActionResult<IEnumerable<Animal>>> Get()
        {
            if (_context.Animals == null)
            {
                return null;
            }
            return await _context.Animals.ToListAsync();
        }
    }
}
