﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AnimalShelter.Models;
using AnimalShelter.Repository;
using Microsoft.AspNetCore.Cors;

namespace AnimalShelter.Controllers
{
     [EnableCors("MyPolicy")]
  //  [DisableCors]
    [System.Web.Mvc.RoutePrefix("api/animals")]
    [Route("api/[controller]")]
    [ApiController]

    public class AnimalsController : ControllerBase
    {
        private readonly AnimalDataLayer _dataLayer;

        public AnimalsController(AnimalDataLayer dataLayer)
        {
            _dataLayer = dataLayer;
        }

        // GET: api/Animals
        [HttpGet]
        [Route("")]
        public async Task<ActionResult<IEnumerable<Animal>>> Get()
        {
            var animals = await _dataLayer.GetAnimals();
            if (animals == null || !animals.Any())
            {
                return NotFound();
            }
            return animals;
        }

        // GET: api/Animals/5
        [HttpGet]
        [Route("{id}")]
        public async Task<ActionResult<Animal>> Get(int id)
        {
            var animal = await _dataLayer.GetAnimal(id);
            if (animal == null)
            {
                return NotFound();
            }
            return animal;
        }

        // GET: api/Animals/Owner/5
        [HttpGet]
        [Route("owner/{owner}")]
        public async Task<ActionResult<IEnumerable<Animal>>> GetByOwner(string owner)
        {
            var animals = await _dataLayer.GetAnimalsByOwner(owner);
            if (animals == null || !animals.Any())
            {
                return NotFound();
            }
            return animals;
        }

        // PUT: api/Animals/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut]
        [Route("{animal}")]
        public async Task<IActionResult> Put(Animal animal)
        {
            var success = await _dataLayer.UpdateAnimal(animal);
            if (!success)
            {
                return NotFound();
            }
            return NoContent();
        }

        // POST: api/Animals
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        [Route("")]
        public async Task<ActionResult<Animal>> Post(Animal animal)
        {
            var id = await _dataLayer.AddAnimal(animal);
            animal.Id = id;

            return CreatedAtAction(nameof(Get), new { id = animal.Id }, animal);
        }

        // DELETE: api/Animals/5
        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var success = await _dataLayer.DeleteAnimal(id);
            if (!success)
            {
                return NotFound();
            }
            return NoContent();
        }
    }

}
