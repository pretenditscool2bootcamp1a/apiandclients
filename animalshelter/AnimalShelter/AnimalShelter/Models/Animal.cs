﻿using System;
using System.Collections.Generic;

namespace AnimalShelter.Models;

public partial class Animal
{
    public int Id { get; set; }

    public string? Name { get; set; }

    public string? Species { get; set; }

    public string? Breed { get; set; }

    public string? Owner { get; set; }

    public DateTime? CheckInDate { get; set; }

    public DateTime? CheckOutDate { get; set; }
}
