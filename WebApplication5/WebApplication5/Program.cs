using Microsoft.EntityFrameworkCore;
using WebApplication5.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using Microsoft.Net.Http.Headers;
using System.Text;

namespace WebApplication5

{
    public class Program
    {

        static void Main(string[] args)
        {
            ConfigurationBuilder configbuilder = new ConfigurationBuilder(); configbuilder.SetBasePath(Directory.GetCurrentDirectory())
.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
            IConfiguration config = configbuilder.Build();

            var builder = WebApplication.CreateBuilder(args);
           // string connectionString = Connections.GetConnectionString();
            // Add services to the container.

            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();
            builder.Services.AddDbContext<NorthwindContext>(
             options => options.UseSqlServer(
             config["ConnectionStrings:Northwind"]
));

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseAuthorization();

            app.MapControllers();

            app.Run();
        }
    }
}